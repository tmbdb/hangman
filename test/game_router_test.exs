defmodule GameRouterTest do
    use ExUnit.Case
    use Plug.Test
  
    @opts GameRouter.init([])
    
    test "testing start_link" do
      assert {:ok, _} = GameRouter.start_link()
    end
  
    test "Reports path / is not served by the application" do
      conn = conn(:get, "/")
      conn = GameRouter.call(conn, @opts)
      assert conn.status == 404
      assert conn.resp_body == "Oops"
    end

    test "Creates a new game" do
        conn = conn(:post, "/games") |> GameRouter.call(@opts)
        assert conn.status == 201
    end

    test "Retrieves game state with nil" do
        conn = conn(:get, "/games/nil") |> GameRouter.call(@opts)
        assert conn.status == 404
    end
    test "Retrieves game state with id" do
        conn = conn(:post, "/games") |> GameRouter.call(@opts)
        {_,location} = List.keyfind(conn.resp_headers,"location",0)
        conn = conn(:get, location) |> GameRouter.call(@opts)
        assert conn.status == 200
    end

    test "Submits player’s guess with nil" do
        conn = conn(:post, "/games/nil/guesses",%{"guess" => "i"}) |> GameRouter.call(@opts)
        assert conn.status == 404
    end

    test "Submits player’s guess with id" do
        conn = conn(:post, "/games") |> GameRouter.call(@opts)
        {_,location} = List.keyfind(conn.resp_headers,"location",0)
        conn = conn(:post, location <> "/guesses",%{"guess" => "i"}) |> GameRouter.call(@opts)
        assert conn.status == 201
    end
    
  end