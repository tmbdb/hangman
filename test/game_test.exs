defmodule GameTest do
    use ExUnit.Case
    import Mock
  
    test "Creates a process holding the game state" do
      with_mock Dictionary, [random_word: fn() -> "platypus" end] do
        {:ok, pid} = Game.start_link()
        assert is_pid(pid)
        assert pid != self()
        assert called Dictionary.random_word
      end
    end
    # some other tests follow
    test "Secret word intilization" do
         assert {:ok, {"hangman", "", "", 9}} == Game.init("hangman")
         assert {:ok, {"johndoe", "", "", 9}} == Game.init("johndoe")
    end 

    test "Check submit guess" do
        {:ok, pid} = Game.start_link()
        assert :ok == Game.submit_guess(pid,"a")
    end 
    
    test "Check feedback" do
        {:ok, pid} = Game.start_link()
        assert  %{feedback: _, remaining_turns: 9, status: :playing} = Game.get_feedback(pid)
    end

    test "Handle Cast" do
        assert {:noreply, {"hangman", "a", "", 8}}  == Game.handle_cast({:submit_guess, "a"}, {"hangman", "", "", 9} )
        assert {:noreply, {"hangman", "ahg", "bxrty", 0}}  == Game.handle_cast({:submit_guess, "y"}, {"hangman", "ahg", "bxrty", 0} )
        assert {:noreply, {"hangman", "ahgnm", "bxr", 0}}  == Game.handle_cast({:submit_guess, "r"}, {"hangman", "ahgnm", "bxr", 0} )
        assert {:noreply, {"hangman", "ahgnm", "b", 2}}  == Game.handle_cast({:submit_guess, "m"}, {"hangman", "ahgn", "b", 3} )
    end
    
    test "Handle Call Lose" do
        {:ok, pid} = Game.start_link()
        assert {:reply, %{feedback: "hang-an", status: :lose, remaining_turns: 0, secret: "hangman"}, {"hangman","hnga","bxcyu",0}}  == Game.handle_call({:get_feedback},pid, {"hangman","hnga","bxcyu",0})
    end

    test "Handle Call Win Situation" do
        {:ok, pid} = Game.start_link()
        assert {:reply, %{feedback: "hangman", status: :win, remaining_turns: 1, secret: "hangman"}, {"hangman","hngam","bxc",1}} == Game.handle_call({:get_feedback},pid, {"hangman","hngam","bxc",1})
    end

    test "Handle Call with playing status" do
        {:ok, pid} = Game.start_link()
        assert {:reply, %{feedback: "hang-an", status: :playing, remaining_turns: 2}, {"hangman","hnga","bxc",2}} == Game.handle_call({:get_feedback},pid, {"hangman","hnga","bxc",2})
    end
  end