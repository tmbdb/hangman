defmodule Hangman do
  def score_guess({secret_word,current_guesses, wrong_guesses,remaining_turns},letter_guess) do
    if remaining_turns == 0 do
        {secret_word,current_guesses, wrong_guesses,remaining_turns}
    else
        if String.contains?(secret_word,letter_guess)  do
            if  String.contains?(current_guesses,letter_guess) do
                {secret_word,current_guesses, wrong_guesses,remaining_turns}
            else
                {secret_word,current_guesses <> letter_guess, wrong_guesses,remaining_turns - 1}
            end
        else
            if  String.contains?(wrong_guesses,letter_guess) do
                {secret_word,current_guesses, wrong_guesses,remaining_turns}
            else
                {secret_word,current_guesses, wrong_guesses <> letter_guess,remaining_turns - 1}
            end
        end
    end
  end

  def format_feedback({secret_word,current_guesses, wrong_guesses,remaining_turns}) do
    list_secret = String.graphemes(secret_word)
    feedback_string = ""
    Enum.map(list_secret, fn x -> if (String.contains?(current_guesses,x) == true) do Enum.concat([feedback_string],[x]) else Enum.concat([feedback_string],["-"]) end end) |> to_string()
  end

end
